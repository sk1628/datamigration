package com.example.demo;

import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.sql.*;
import java.util.*;
import java.util.concurrent.CompletableFuture;


@Data
@Component
@ConfigurationProperties(prefix = "db.full.migrate.tables")
class FullMigratedTables {
    private List<String> names;
}

@Component
public class TableMigrate {

    private static final Logger logger = LoggerFactory.getLogger(TableMigrate.class);
    private static final Logger data_logger = LoggerFactory.getLogger("data-log");
    private final long STATUS_PENDING = 0;
    private final long STATUS_COMPLETE = 1;
    private final long STATUS_FAILED = 2;
    private final long STATUS_PARTIAL = 3;
    @Autowired
    Connection source;
    @Value("${db.batchSize}")
    private String dbBatchSize;
    @Value("${es.bulkSize}")
    private String esBulkSize;

    @Value("${db.migrate.limit.max}")
    private long maxRowMigration;

    private final List<String> changeColmnsName = Arrays.asList("name","fname","mname");
    private final List<String> changeColmns = Arrays.asList("roll_no","regno");

    private List<String> fullMigratedTablesNames;

    public TableMigrate(FullMigratedTables fullMigratedTables) {
        fullMigratedTablesNames = fullMigratedTables.getNames();
    }

    private static String getNumericString(String str) {
        if (null == str || str.isEmpty())
            return "";
        return str.replaceAll("[^0-9]", "");
    }

    private long findNumberOfRowToMigrate(String tableName,long total)
    {
        if(fullMigratedTablesNames.contains(tableName)) return total;
        return Math.min(total,maxRowMigration);
    }

    public long findTotalRows(String tableName) throws SQLException {

        String ret = null;
        long mx = 0;
        long total = 0;
        try {
            String countRowQuery = SqLDateFormatQueryUtils.generateTotalRowQuery(tableName);
            PreparedStatement s1 = source.prepareStatement(countRowQuery, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs1 = s1.executeQuery();
            if (rs1.next()) {
                total = Long.parseLong(rs1.getString("CNT"));
            }
            return total;

        } catch (Exception e) {
            return 0;
        }
    }

    public void migrateTable(String tableName) {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start("Migrating table name " + tableName);
            long limit = Long.valueOf(esBulkSize).longValue();
            long offset = 0;
            long total = findNumberOfRowToMigrate(tableName,findTotalRows(tableName));
            String dateFormat = null;
            boolean fullComplete = true;
            boolean partialComplete = false;
            while (offset <= total) {
                long newLimit=Math.min(total-offset,limit);
                boolean complete = migrateTablePart(tableName, newLimit, offset);
                if (complete) partialComplete = true;
                else fullComplete = false;
                offset += newLimit;
                if(offset>=total)
                    break;

            }
            if (fullComplete)
                logger.info("table " + tableName + "complete successfully");
            else if (partialComplete)
                logger.error("table " + tableName + "complete partially");
            else
                logger.error("table " + tableName + " failed");
            stopWatch.stop();
            logger.info(stopWatch.prettyPrint());
        } catch (Exception e) {
            logger.error("table " + tableName + " failed");
        }

    }

    public boolean migrateTablePart(String tableName, long limit, long offset) {
        try {
//            if(tableName.equals("jsc_dhaka_2015"))
//                System.out.println(tableName);
            List<List<String>> records = new LinkedList<>();


            List<String> columns = new ArrayList<>();


            String query = "select * from " + tableName + " LIMIT " + limit + " OFFSET " + offset;
            PreparedStatement s = source.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = s.executeQuery();



            ResultSetMetaData metadata = rs.getMetaData();
            for (int i = 1; i <= metadata.getColumnCount(); i++) {
                columns.add(metadata.getColumnName(i));
            }
            if(offset==0)
                records.add(columns);
            while (rs.next()) {
                List<String> row = new ArrayList<>();
                for (String cl : columns) {

                    String val = rs.getString(cl);
                    if(changeColmnsName.contains(cl))
                        val = getRandomStringForNames();
                    if(changeColmns.contains(cl)) {
                        // val = getRandomStringForNames();    // samin
                        val=SqlDumpUtils.getRandom(cl);
                    }
                    row.add(val);

                }
                records.add(row);
            }
            SqlDumpUtils.genCsv(records,tableName);
            records.clear();
            //samin code  push in csv
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("error in bulk migrating for table " + tableName, ex);
            return false;
        }
    }


    public String getRandomStringForNames() {
        return "";
    }

    public CompletableFuture<Void> migrateTableAsync(String tableName) {
        Runnable runnable = () -> {
            migrateTable(tableName);
        };
        return CompletableFuture.runAsync(runnable);
    }


}
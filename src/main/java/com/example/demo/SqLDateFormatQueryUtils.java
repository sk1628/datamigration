package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SqLDateFormatQueryUtils {
    private static final Logger logger = LoggerFactory.getLogger(SqLDateFormatQueryUtils.class);


    public static String COUNT_MATCHED_BY_REGEX="select COUNT(*) as CNT from {TABLE_NAME} where dob REGEXP {REGEX_STRING}";
    public static String COUNT_TOTAL_ROW="select COUNT(*) as CNT from {TABLE_NAME}";
    public static String ddMMyy="'^(0?[1-9]|[12][0-9]|3[01])(0?[1-9]|1[012])[0-9][0-9]$'";
    public static String MM_dd_yyyy="'^(0?[1-9]|1[012])[-/.](0?[1-9]|[12][0-9]|3[01])[-/.](19|20)[0-9][0-9]$'";
    public static String dd_MM_yyyy="'^(0?[1-9]|[12][0-9]|3[01])[-/.](0?[1-9]|1[012])[-/.](19|20)[0-9][0-9]$'";
    public static String yyyy_mm_dd="'^(19|20)[0-9][0-9][-/.](0?[1-9]|1[012])[-/.](0?[1-9]|[12][0-9]|3[01])$'";

    public static String generateTotalRowQuery(String tableName)
    {
        String query=COUNT_TOTAL_ROW.replace("{TABLE_NAME}",tableName);
        return query;
    }


}

package com.example.demo;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Component
public class MigrationWork {

    private static final Logger logger = LoggerFactory.getLogger(MigrationWork.class);

    private final long STATUS_PENDING = 0;
    private final long STATUS_COMPLETE = 1;
    private final long STATUS_FAILED = 2;

    @Autowired
    DbService dbService;

    @Value("${async.BatchSize}")
    private String asyncBatchSize;


    @PostConstruct
    public void run() throws Exception {

        //here order matters
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("starting migration bismillahir rahmanir rahim");

        createCsvfilesForTables(getAllTables());
        stopWatch.stop();
        logger.info(stopWatch.prettyPrint());
        System.exit(0);

    }

    private List<String> getAllTables()  throws Exception {
        logger.info("starting getting table names ");

       return   dbService.getTableNames();
    }



    private void createCsvfilesForTables(List<String> tableRecords) throws Exception {
        logger.info("starting migrating student tables to es");
        System.out.println(asyncBatchSize);
        List<CompletableFuture> list = new ArrayList<>();
        tableRecords.stream().forEach(k -> {
                list.add(dbService.migrateTableAsync(k));

            if (list.size() >= Integer.valueOf(asyncBatchSize).intValue()) {
                try {
                    CompletableFuture<Void> allFutures = CompletableFuture.allOf(
                            list.toArray(new CompletableFuture[list.size()]));
                    allFutures.get();
                    list.clear();
                } catch (Exception ex) {
                    logger.error("error occured method : migrateStudents()  " + ex);
                }
            }
        });
        if (list.size() >= 1) {
            try {
                CompletableFuture<Void> allFutures = CompletableFuture.allOf(
                        list.toArray(new CompletableFuture[list.size()]));
                allFutures.get();
                list.clear();
            } catch (Exception ex) {
                logger.error("error occured method : migrateStudents()  " + ex);
            }
        }
        logger.info("starting migrating student tables to es -- COMPLETE");

    }
}

package com.example.demo;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class SqlDumpUtils {
    static public void genCsv(List<List<String>> listList, String tableName) throws IOException {
        String fileName="Info/"+tableName+".csv";
        FileWriter fw = new FileWriter(fileName, true);
        for(List<String> list:listList)
        {
            String line="";
            for(int i=0;i<list.size();i++)
            {
                String value="";
                if(list.get(i)!=null && !list.get(i).isEmpty())
                {
                    value="\""+list.get(i)+"\"";
                }
                if(i!=0)
                {
                    line+=",";
                }
                line+=value;
            }
            line+="\n";
            fw.append(line);
        }
        fw.close();
    }
    static String getDigit(int digit)
    {
        digit--;
        int randomNum = ThreadLocalRandom.current().nextInt((int)Math.pow(10,digit),10*(int)Math.pow(10,digit));
        return randomNum+"";
    }
    static String getRandom(String fieldName)
    {
        return "";
    }
}

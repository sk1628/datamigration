package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class DbService {

    @Autowired
    TableNames tableNames;
    @Autowired
    TableMigrate tableMigrate;

    public List<String> getTableNames() {
           return tableNames.getTables() ;
    }

    public void migrateTable(String tableName) {
        tableMigrate.migrateTable(tableName);
    }

    public CompletableFuture<Void> migrateTableAsync(String tableName) {
        return tableMigrate.migrateTableAsync(tableName);
    }
}

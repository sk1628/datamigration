package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Component
public class TableNames {

    private static final Logger logger = LoggerFactory.getLogger(TableNames.class);
    private List<String> BOARDS;
    @Autowired
    Connection source;

    @Value("${edu.data.schema.name}")
    private String tableSchema;


    public List<String> getTables() {

        try {
            List<String> list = new ArrayList<>();
            //    Connection source = DriverManager.getConnection(databaseProperties.getUrl(), databaseProperties.getUsername(), databaseProperties.getPassword());
            String query = "SELECT table_name FROM information_schema.tables WHERE table_schema ='"+tableSchema+"';";

            PreparedStatement s = source.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = s.executeQuery();
            while (rs.next()) {
                String tableName = rs.getString("table_name");
                    list.add(tableName.toLowerCase());
            }
            return list;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }


}
